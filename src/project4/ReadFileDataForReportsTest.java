package project4;

import static org.junit.Assert.*;

import java.io.IOException;
import java.util.InputMismatchException;
import java.util.NoSuchElementException;

import org.junit.Test;

public class ReadFileDataForReportsTest {
    private ReadFileDataForReports myReports = new ReadFileDataForReports();

	@Test
	public void testForSuccessProviderName() {
		String memProviderName = "";
		memProviderName = myReports.findProviderNameForMemberReport("812384375");		
		assertEquals("Tony Stark", memProviderName);
	    }
	
	@Test(expected = NoSuchElementException.class)
	public void testForExceptionProviderName() {
		String memProviderName = "";
		memProviderName = myReports.findProviderNameForMemberReport("35409");
	}
	
	@Test(expected = Error.class)
	public void testForFailureProviderName() {
		String memProviderName = "";
		memProviderName = myReports.findProviderNameForMemberReport(15);
	}
}
