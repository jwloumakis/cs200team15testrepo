package project4;

import static org.junit.Assert.*;

import java.io.File;
import java.util.InputMismatchException;
import org.junit.Test;

/**
 * JUnit tests for the ValidateProvider method of the PerformTerminalActions class. 
 * @author Pierce McLawhorn
 */

public class ValidateProviderTest {

	PerformTerminalActions pta = new PerformTerminalActions();
	File proList = new File("ProviderList.txt");

	@Test
	public void testValidateProviderSuccess() {
		assertTrue(pta.validateProvider(432957104, proList));
	}
	
	@Test (expected = InputMismatchException.class)
	public void testValidateProviderFailure() {
		//Fail test case with <9 numbers
		assertFalse(pta.validateProvider(0, proList));
	}
	
	@Test (expected = InputMismatchException.class)
	public void testValidateProviderFailure2() {
		//Fail test case with >9 numbers
		assertFalse(pta.validateProvider(1234567890, proList));
	}
	
	@Test (expected = InputMismatchException.class)
	public void testValidateProviderFailure3() {
		//Fail test case with =9 numbers, but the provider is not valid
		assertFalse(pta.validateProvider(111111111, proList));
	}
	
	@Test (expected = NumberFormatException.class)
	public void testValidateProviderFailure4() {
		//Fail test case with alphanumeric characters
		String test = new String("12345a789");
		assertFalse(pta.validateProvider(Integer.parseInt(test), proList));
	}

	@Test (expected = NullPointerException.class)
	public void testValidateProviderFailure5() {
		//Fail test case with the wrong file
		File testList = new File("ProvList.txt");
		assertFalse(pta.validateProvider(432957104, testList));
	}
}
