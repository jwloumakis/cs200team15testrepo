package project4;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Scanner;
/**
 * MainAccountingProcedure is where Provider Reports, Member Reports and Summury Reports get generated.
 * 
 * 
 * @author apurvapatel
 * @version 1.0
 */


public class MainAccountingProcedure {
	
	Scanner scanner = new Scanner(System.in);
	RunAllReports runner = new RunAllReports();
	
	public void Choice() {
		int choice = 0;
		do {
		StringBuilder choicesOption = new StringBuilder();
		choicesOption.append("\n**************Generating Reports**************\n");
		choicesOption.append("Please Select from below options: \n");
		choicesOption.append("1. Run all Member report. \n");
		choicesOption.append("2. Run all Provider report. \n");
		choicesOption.append("3. Run Summary report. \n");
		choicesOption.append("4. Run Individual Member Report. \n");
		choicesOption.append("5. Run Individual Provider Report. \n");
		choicesOption.append("0. To return to main screen.\n");
		
			System.out.println(choicesOption.toString());
			choice = scanner.nextInt();
			
			if ((choice < 0) || (choice > 5)) {
				System.out.println("Invalid Input. Please enter right input.");
				Choice();
			}
			else {
				switch (choice) {
				case 0:
					System.out.println("Returning to main screen");
					break;
				case 1:
					System.out.println("Generating all member report.");
					generateAllMemberReport();
					break;
				case 2:
					generateAllProviderReport();
					break;
				case 3:
					generateSummaryReport();
					break;
				case 4: 
					generateIndividualMemberReport();
					break;
				case 5: 
					generateIndivudualProviderReport();
					break;
				} 
			}	
		}while (choice != 0);
	}
	
	//In RunAllReports.java it calls generate all member reports
	private void generateAllMemberReport () {
		runner.runAllMemberReports();
	}
	
	//In RunAllReports.java it calls generate all provider reports
	private void generateAllProviderReport () {
		runner.runAllProviderReports();
	}

	//In RunAllReports.java it calls generate summury reports
	private void generateSummaryReport () {
		runner.runAllSummaryReports();
	}

	//In RunAllReports.java it calls generate individual member reports
	private void generateIndividualMemberReport () {
		runner.runIndividualMemberReports();
	}
	
	//In RunAllReports.java it calls generate individual provider reports
	private void generateIndivudualProviderReport () {
		runner.runIndividualProviderReports();
	}
}



