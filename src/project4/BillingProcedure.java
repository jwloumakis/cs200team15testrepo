package project4;
import java.io.*;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

/**
 * The Billing Procedure Class. The methods of this class are designed to carry
 * out all the necessary functions of the billing procedure, including looking up the
 * service fee and sending a report to a file. 
 * @author Pierce McLawhorn
 */

public class BillingProcedure {
  
  private String serviceDate;	 
  private int providerNum;
  private int memberNum;
  private int serviceCode;
  private double serviceFee;
  private int temp;
  private String comments = "None";
  
  Scanner userInput = new Scanner(System.in);
  
  /** 
   * This is the default constructor
   */
  
  public BillingProcedure() {
    	System.out.println("Error in constructor: use int providerNum parameter");
  }
    
  /** 
   * This is the intended constructor
   * @param num the provider number for the provider whom is billing chocAn
   */
  
  public BillingProcedure(int num) {
    this.providerNum = num;
  }
  
  /** 
   * Method to set the member number
   * @param num the member number for the member being billed
   */
  
  public void setMemberNumber(int num) {
    this.memberNum = num;
  }
  
  /** 
   * Method to prompt the user for the service date and store it
   * @return a string corresponding to the date a particular service was provided
   */
  
  public String getDate() {
    boolean validDate = false;	
    // Loop runs until valid date is entered
    do {	
      System.out.println("Enter Date of Service (MM-DD-YYYY) :");
      String input = userInput.nextLine();
      if (input.matches("[0-9]{2}-[0-9]{2}-[0-9]{4}")) {
	    try {   
	      SimpleDateFormat sdf = new SimpleDateFormat("MM-dd-yyyy");
	      Date date = sdf.parse(input);          
	      Calendar cal = Calendar.getInstance();
	      cal.setTime(date);
	      // Split basic input by the delimiter "-"
	      String parts[] = input.split("-");
	      boolean valid = true;
	      // Check each part of input
	      if (Integer.parseInt(parts[0]) != cal.get(Calendar.MONTH) + 1) {
	        valid = false;
	        System.out.println(parts[0] + " is not a valid month");
	      } 
	      if (Integer.parseInt(parts[1]) != cal.get(Calendar.DATE)) {
              valid = false;
              System.out.println(parts[1] + " is not a valid day of the month");
          } 
          if (Integer.parseInt(parts[2]) != cal.get(Calendar.YEAR)) {
              valid = false;
              System.out.println(parts[2] + " is not a valid year");
          }            

	      if (valid) {
	        this.serviceDate = (new SimpleDateFormat("MM-dd-yyyy").format(date)).toString();
	        validDate = true;
	      }
	    } catch (ParseException ex) {
	        System.out.println("Unable to parse " + input + "; invalid format");
	      }
	  } else {
	      System.out.println("Invalid date, please try again.");
	    }
    } while (validDate == false);
   
	  return this.serviceDate;      	
  }
  
  /** 
   * This method prompts the user for the service code and stores it
   * @return an integer corresponding to the service code for a particular service
   */
  
  public int getServiceCode() {  
    System.out.println("Enter Service Code :");
    this.serviceCode = 0;
	do {
		String testIn = userInput.nextLine();
		testIn = testIn.substring(0, Math.min(testIn.length(), 6));
		int n;
		if (testIn.matches("^[0-9]+$") && ((n = Integer.parseInt(testIn)) >= 100000) && (n <= 999999)) this.serviceCode = n;
		else {
			System.out.println("Invalid input. Please enter a six-digit service code.");
			try {
				TimeUnit.SECONDS.sleep((long) 2.5);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	} while (this.serviceCode == 0);
    
	return this.serviceCode;      	
  }
  
  /**
   * This method does looks to see if the service code exists in the provider directory
   * @return a boolean corresponding to whether the service code exists or not
   * @throws FileNotFound Exception if the Provider Directory file is not found
   */
  
  public boolean lookupServiceCode(int code, File file) throws FileNotFoundException {
	this.temp = 0;
	Scanner input = new Scanner(file);
	  
	while(input.hasNextLine()) { //loop to lookup service name corresponding to code
	  this.temp = input.nextInt();
      if(this.temp == code) {
	    input.close();
		return true;			
	  }
	  else {
	    input.next();
		input.next();
	  }
	}
			
	if(this.temp != code) {
	  System.out.println("Error: Nonexistent Code.");
	  input.close();
	  return false;
	}
	
    input.close();
    return false;
  } 
  
  /** 
   * This method looks up the service name corresponding to the service code
   * and prompts the user to verify that the name is correct.
   * @return a boolean corresponding to whether the service was validated or not
   * @throws FileNotFound Exception if the Provider Directory file is not found 
   */
  
  public boolean userValidate(File file) throws FileNotFoundException {
	this.temp = 0;
	
	Scanner input = new Scanner(file);
	
    while(input.hasNextLine()) { //loop to lookup service name corresponding to code
	  this.temp = input.nextInt();
	  if(this.temp == this.serviceCode) {
	    System.out.println(this.serviceCode + " is the code for: " + input.next());
	    System.out.println("Is this the correct service? (Y or N)");
	    if(!userInput.nextLine().toUpperCase().equals("Y")) {
	    	  System.out.println("Wrong Service? Try entering the code again.");
	    	  input.close();
	    	  return false;
	    }
		break;			
      }
	  else {
	    input.next();
		input.next();
	  }
    }
	    
	System.out.println("Would you like to enter comments? (Y or N)");
    if(userInput.nextLine().toUpperCase().equals("Y")) {
    	  System.out.println("Enter comments (press return when finished): ");
    	  this.comments = userInput.nextLine();
    	  if(this.comments.length() > 200) {
    		this.comments = this.comments.substring(0, Math.min(this.comments.length(), 200));
    	  }
    }
    
    input.close();
    return true;
  }
  
  /** 
   * This method outputs a record to a file named "EFTData". Includes the current
   * date and time.
   */
  
  public void writeDataToDisk() {
    PrintWriter writer = null;
	try {
	    writer = new PrintWriter(new FileWriter("EFTData", true));
	} catch (FileNotFoundException e) {
		e.printStackTrace();
	} catch (UnsupportedEncodingException e) {
		e.printStackTrace();
	} catch (IOException e) {
		e.printStackTrace();
	}
	
	DateFormat dateFormat = new SimpleDateFormat("MM-dd-yyyy HH:mm:ss");
	Calendar cal = Calendar.getInstance();
	writer.println("CurrentDateAndTime\t" + dateFormat.format(cal.getTime()));
	
    writer.println("DateOfServiceProvided  " + this.serviceDate);
    writer.println("ProviderNumber  " + this.providerNum);
    writer.println("MemberNumber  " + this.memberNum);
    writer.println("ServiceCode  " + this.serviceCode);
    writer.println("Comment  " + this.comments);
    writer.println("END");
    
    writer.close();
    
    return;
  }
  
  /** 
   * This method searches the provider directory for the fee to be paid corresponding to a
   * particular service code, and displays the fee on the screen.
   * @return double storing the fee to be paid
   * @throws FileNotFoundException if the ProviderDirectory file is not located
   */
  
  public double getServiceFee() throws FileNotFoundException {
	double temp = 0.0;
	File file = new File("ProviderDirectory.txt");
	Scanner fileScan = new Scanner(file);
	
    while(fileScan.hasNextLine()) { //loop to lookup fee corresponding to code
	  temp = fileScan.nextInt();
	  if(temp == this.serviceCode) { //code should already be validated before this method is used
		fileScan.next();
		this.serviceFee = fileScan.nextDouble();
	    System.out.println("The fee for this service is $" + this.serviceFee);
		break;	
      }	
	  else {
		fileScan.next(); 
		fileScan.next();
	  }
    }
    fileScan.close();
	return this.serviceFee;
  }
  
  /** 
   * This method prompts the user to fill out the provider verification form
   * from the command line.
   */
  
  public void fillOutForm() {
	  	 
	  System.out.println("\n*PROVIDER VERIFICATION FORM*\n");
	  System.out.println("Enter current date and time (MM-DD-YYYY hh:ss:mm): ");
	  userInput.nextLine();
	  System.out.println("\nEnter date of service (MM-DD-YYYY): ");
	  userInput.nextLine();
	  System.out.println("\nEnter member name: ");
	  userInput.nextLine();
	  System.out.println("\nEnter member number: ");
	  userInput.nextLine();
	  System.out.println("\nEnter service code: ");
	  userInput.nextLine();
	  System.out.println("\nEnter fee to be paid: ");
	  userInput.nextLine();
	  
  } 
}	
  


	
