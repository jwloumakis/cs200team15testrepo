package project4;
import java.util.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Scanner;

import javax.print.DocFlavor.URL; 

/**
 * ReadFileDataForReports reads in four files EFTData, ProviderList.txt, MemberList.txt, ProviderDirectory.text
 * To generate Reports.
 *
 * @author apurvapatel
 * @version 1.0
 */

public class ReadFileDataForReports {
	private Scanner EFTDataFile;  
	private Scanner MemberFile;
	private Scanner ProviderFile;
	private Scanner ProviderListFile;
	private Scanner ProviderDirectoryFile;
	
	// Arraylist using to scan in all valuable things from EFTData file.
	ArrayList<String> eftData = new ArrayList<String>(); 
	
	//This is the information needed to create member report.
	String memName, memNum, memStreet, memCity, memState, memZip, memDateOfService, memProviderName, memServiceName;

	//Opening EFTData file to start reading from file.
	//@throws [FileNotFoundException] if EFTData file is not there.
	public void openEFTDataFile () {
		try {
			EFTDataFile = new Scanner (new File ("EFTData"));
		}
		catch (Exception e) {
			System.out.println("No file named EFTData exists in system.");
		}
	}
	
	//This function generate the all member reports.
	public void readFileForGenerateAllMemberReports () {	
		eftData.clear(); 								// Clearing out priviously scaned eft data.
		while (EFTDataFile.hasNext()) {
            String s = EFTDataFile.next();
            		eftData.add(s);							//Reading data from EFTDataFile
            }
        for (int i = 0; i < eftData.size(); i++) {
        		if(eftData.get(i).equals("DateOfServiceProvided")) {
        			memDateOfService = eftData.get(i+1);	
        		}
        		else if (eftData.get(i).equals("ProviderNumber")) {
        		 memProviderName =	findProviderNameForMemberReport(eftData.get(i+1));	 
        		}
        		else if (eftData.get(i).equals("MemberNumber")) {
        			findMemberInfoForMemberReport(eftData.get(i+1));
        			memNum = eftData.get(i+1);
        		}
        		else if (eftData.get(i).equals("ServiceCode")) {
        		memServiceName  =findServiceNameForMemberReport(eftData.get(i+1));
        		printAllMemberReport(); 					 //Printing all member reports once getting all inputs.
        		}
        } 
	}
	
	//Closing all files after done working with them.
	public void closeFileForGenerateAllMemberReports () {
		EFTDataFile.close();
		MemberFile.close();
		ProviderListFile.close();
		ProviderDirectoryFile.close();
	}
	
	/*Reading member informations from MemberList.txt File.
	*@param [memberNumber] is a member number is to search member info from MemberList.txt
	*@throws [FileNotFoundException] if MemberList.txt not found.
	*/
	void findMemberInfoForMemberReport(String memberNumber) {
		String line;
		try {
			MemberFile = new Scanner (new File ("MemberList.txt")); //Opening MemberList.txt file here.
		}
		catch (Exception e) {
			System.out.println("No file named MemberFile exists in system.");
		}
		while (MemberFile.hasNextLine()) {
			line = MemberFile.nextLine();
			if (line.equals(memberNumber)) {
				String s1 = MemberFile.nextLine();
				String s2 = MemberFile.nextLine();
				memName = s1 + " " + s2;
				memStreet = MemberFile.nextLine();
				memCity = MemberFile.nextLine();
				memState = MemberFile.nextLine();
				memZip = MemberFile.nextLine();
				break;
			}
		}
	}
	
	/*Find Provider name for member reports. This method receives in provider number to search in ProviderList.txt file.
	 * @param[providerNumber] is a provider number to search for provider info from providerList.txt
	 * @throws[Filenotfoundexceptipn] if file is not presetn.
	 */
	public String findProviderNameForMemberReport (String providerNumber) {
		String number;
		String namef, namel, nameFull = "";
		try {
			ProviderListFile = new Scanner (new File ("ProviderList.txt")); //Opening ProviderList.txt file
		}
		catch (Exception e) {
			System.out.println("No file named ProviderList exists in system.");
		}
		while (ProviderListFile.hasNext()) {
			number = ProviderListFile.next();
			if (providerNumber.equals(number)) {
				namef = ProviderListFile.next();
				namel = ProviderListFile.next();
				nameFull = namef + " " + namel;
				break;
			}
		}
		return nameFull;
	}
	
	/*Finding service name for memberReports. It receives in service code to search service name from ProviderDirectory file.
	 * @param[ServiceCode] is for to search service name from ProviderDirectory.txt
	 * @throws[FileNotFoundExceptions] if providerDirectory.txt is not present.
	 */
	public String findServiceNameForMemberReport(String serviceCode) {
		String code, Servicename = "";
		try {
			ProviderDirectoryFile = new Scanner (new File ("ProviderDirectory.txt")); //Opening ProviderDirectory.txt
		}
		catch (Exception e) {
			System.out.println("No file named Provider Directory exists in system.");
		}
		while (ProviderDirectoryFile.hasNext()) {
			code = ProviderDirectoryFile.next();
			if (code.equals(serviceCode)) {
				Servicename =  ProviderDirectoryFile.next();
			}
		}
		return Servicename;
	}
	
	void printAllMemberReport() {	 
		//Writing generated reports to the text file EmailMemberReports/Membernumber.txt
		// Creating folder Email Member Reports where it store all 
		//File name is member numbers.
		File f = new File(System.getProperty("user.dir")+"/Email Member Reports/"+ memNum + ".txt"); 
	    if(!f.getParentFile().exists()){
	        f.getParentFile().mkdirs();
	    }    if(!f.exists()){
	        try {
	            f.createNewFile();
	        } catch (Exception e) {
	            e.printStackTrace();
	        }
	    }
	    
	    //Wrtiting to the files.
	    //Catchinc an exceptions if coundn't write to file.
	    try {
	        File dir = new File(f.getParentFile(), f.getName());
	        PrintWriter writer = new PrintWriter(dir);
	        writer.printf("Name : %s\n"
			+ "Member Number: %s \n"
			+ "Address: %s \n"
			+ "%s %s, %s\n"
			+ "Date of Service Provided: %s \n"
			+ "Provider Name: %s\n"
			+ "Service Name: %s\n", memName, memNum, memStreet, memCity, memState, memZip, memDateOfService, memProviderName, memServiceName);
	        writer.close();
	    } catch (Exception e) {
	        e.printStackTrace();
	    } 
		//Printingg member report to the screen.
		System.out.printf("\n\nName : %s\n"
						+ "Member Number: %s \n"
						+ "Address: %s \n"
						+ "%s %s, %s\n"
						+ "Date of Service Provided: %s \n"
						+ "Provider Name: %s\n"
						+ "Service Name: %s\n", memName, memNum, memStreet, memCity, memState, memZip, memDateOfService, memProviderName, memServiceName);
	}
	
	//Generate All Provider Reports -**-
	
	ArrayList<String> providerTracker = new ArrayList<String>(); //Keep track of howmany reports to be generated 
	//Informations needed to write prover reports
	String proName, proNum, proStreet, proCity, proState, proZip, proDateOfService, proDateEntered, proTimeEntered, proMemberName, proMemberNumber, proServiceCode;
	String proFeeToBePaid;
	int iHolder = 0;
	
	/*Provider Reports, Individual Reports and Summury Reports Receives same file inputs and reads same files so function readFileForGenerateALlProvider reports 
	 * serving for them too.
	 * 
	 * @param checkWhichReport is either "ProviderReports", "SummuryReports", "ProviderReports"
	 * @param generateForThis is either "0" or "providerNumber"
	 * 
	 * if checkWhichReport is "ProviderReports" generateForThis == "0" will generate provider reports
	 * if checkWhichReport is "SummuryReports" generateForThis == "0" will generate Summury reports
	 * if checkWhichReport is "ProviderReports" generateForThis == "providerNumbers" will generate Individual provider reports
	 */
	
	public void readFileForGenerateAllProviderReports (String checkWhichReport, String generateForThis) {	
		eftData.clear();
		pname.clear();
		pnum.clear();
		pstreet.clear();
		pcity.clear();
		pstate.clear();
		pzip.clear();
		pdos.clear();
		pdateEntered.clear();
		ptimeEntered.clear();
		pmemname.clear();
		pmemnumber.clear();
		pservicecode.clear();
		pfeetobepaid.clear();
		providerTracker.clear();
		while (EFTDataFile.hasNext()) {
            String s = EFTDataFile.next();
            		//System.out.printf("%s", s);
            eftData.add(s);
		}
	for (int i = 0; i < eftData.size(); i++) {
		if(eftData.get(i).equals("DateOfServiceProvided")) {
			proDateOfService = eftData.get(i+1);
    		}
		else if (eftData.get(i).equals("CurrentDateAndTime")) {
		proDateEntered = eftData.get(i+1);
		proTimeEntered = eftData.get(i+2);
		}
    		else if (eftData.get(i).equals("ProviderNumber")) {
    			findProviderInfoForProviderReport(eftData.get(i+1));
    			proNum = eftData.get(i+1);
    			if (providerTracker.contains(proNum)) {
    			}
    			else {
    				providerTracker.add(proNum);
    			}
    		}
    		else if (eftData.get(i).equals("MemberNumber")) {
    			proMemberNumber = eftData.get(i+1);
    			findMemberInfoForProviderReport(eftData.get(i+1));
    		}
    		else if (eftData.get(i).equals("ServiceCode")) {
    			proServiceCode = eftData.get(i+1);
    			findServiceFeeForProviderReport(proServiceCode);   		
    			}
		}
	if (checkWhichReport == "ProviderReport" && generateForThis == "0") { //Generating all providers reports
	populateArraylist("ProviderReport", generateForThis); //Printing to file
	}
	else if (checkWhichReport == "ProviderReport" && generateForThis != "0") {
		populateArraylist("ProviderReport", generateForThis);  //Generating individual provider reports
	}
	else {
		populateArraylist("SummaryReport", generateForThis);  //Generating Summary reports
	}
	}
	
	public void closeFileForGenerateAllProviderReports () {
		EFTDataFile.close();
	}
	
	//Finding provider info for provider reports. Reading data from ProviderList.txt
	void findProviderInfoForProviderReport(String providerNumber){
		String line;
		try {
			ProviderFile = new Scanner (new File ("ProviderList.txt"));  //Opening providerlist.txt file
		}
		catch (Exception e) {
			System.out.println("No file named ProviderTxt exists in system.");
		}
		while (ProviderFile.hasNextLine()) {
			line = ProviderFile.nextLine();
			if (line.equals(providerNumber)) {
				String s1 = ProviderFile.nextLine();
				String s2 = ProviderFile.nextLine();
				proName = s1 + " " + s2;
				proStreet = ProviderFile.nextLine();
				proCity = ProviderFile.nextLine();
				proState = ProviderFile.nextLine();
				proZip = ProviderFile.nextLine();
				//System.out.printf("%s \n %s \n %s \n %s \n %s \n", proName, proStreet, proCity, proState, proZip);
				break;
			}
		}
		ProviderFile.close();
	}
	
	//Find member info for provider reports. receives in memberNumber parameter to search out member name.
	void findMemberInfoForProviderReport (String memberNumber) {
		String line;
		try {
			MemberFile = new Scanner (new File ("MemberList.txt"));
		}
		catch (Exception e) {
			System.out.println("No file named EFTData exists in system.");
		}
		while (MemberFile.hasNextLine()) {
			line = MemberFile.nextLine();
			if (line.equals(memberNumber)) {
				String s1 = MemberFile.nextLine();
				String s2 = MemberFile.nextLine();
				proMemberName = s1 + " " + s2;				
				break;
			}
		}
		MemberFile.close();
	}
	
	//Finding service fee from providerDirectory file receives in service code to search service fee.
	void findServiceFeeForProviderReport(String serviceCode) {
		String code;
		try {
			ProviderDirectoryFile = new Scanner (new File ("ProviderDirectory.txt"));
		}
		catch (Exception e) {
			System.out.println("No file named EFTData exists in system.");
		}
		while (ProviderDirectoryFile.hasNext()) {
			code = ProviderDirectoryFile.next();
			if (code.equals(serviceCode)) {
				String no_use = ProviderDirectoryFile.next();
				proFeeToBePaid = ProviderDirectoryFile.next();
				printWithProviderInfoInReport();
			}
		}
		ProviderDirectoryFile.close();
	}
	//Storing all provider reports to the array and later accessing it to generate Email reports.
	ArrayList<String> pname = new ArrayList<String>();
	ArrayList<String> pnum = new ArrayList<String>();
	ArrayList<String> pstreet = new ArrayList<String>();
	ArrayList<String> pcity = new ArrayList<String>();
	ArrayList<String> pstate = new ArrayList<String>();
	ArrayList<String> pzip = new ArrayList<String>();
	ArrayList<String> pdos = new ArrayList<String>();
	ArrayList<String> pdateEntered = new ArrayList<String>();
	ArrayList<String> ptimeEntered = new ArrayList<String>();
	ArrayList<String> pmemname = new ArrayList<String>();
	ArrayList<String> pmemnumber = new ArrayList<String>();
	ArrayList<String> pservicecode = new ArrayList<String>();
	ArrayList<String> pfeetobepaid = new ArrayList<String>();
	
	/**Generating Provider Reports // Summary reports// Indivudual Provider Reports. Depends on parameter that
	 * 
	 * @param checkWhichReport is either "ProviderReports", "SummuryReports", "ProviderReports"
	 * @param generateForThis is either "0" or "providerNumber"
	 * 
	 * if checkWhichReport is "ProviderReports" generateForThis == "0" will generate provider reports
	 * if checkWhichReport is "SummuryReports" generateForThis == "0" will generate Summury reports
	 * if checkWhichReport is "ProviderReports" generateForThis == "providerNumbers" will generate Individual provider reports
	 */
	
	void populateArraylist (String checkWhichReport, String generateForThis) {
		if (checkWhichReport == "ProviderReport" && generateForThis == "0") {
		int numOfConsultations = 0;
		double totalFeeForWeek = 0.0;
		for (int i = 0; i < providerTracker.size(); i++) { //Provider Tracker keeps track of howmany provider have provided services to members.
		System.out.printf("\n---------------------------    %d      -----------\n", i);
			System.out.printf("Provider Name: %s\n"
					+ "Provider Number: %s\n"
					+ "Provider Address: %s\n"
					+ "%s %s, %s \n"
					+ "Date of service: %s\n"
					+ "Date and time received by computer: %s %s\n"
					+ "Member Name: %s\n"
					+ "Member Number: %s \n"
					+ "Service Code: %s\n"
					+ "Fee to be paid: %s\n",pname.get(i), pnum.get(i), pstreet.get(i), pcity.get(i), pstate.get(i), pzip.get(i), pdos.get(i), pdateEntered.get(i), ptimeEntered.get(i), pmemname.get(i), pmemnumber.get(i), pservicecode.get(i), pfeetobepaid.get(i));

			numOfConsultations += 1;			
			totalFeeForWeek = totalFeeForWeek + Double.parseDouble( pfeetobepaid.get(i));
			for (int j = i+1; j < pname.size(); j++) {
				if (pnum.get(j).equals(pnum.get(i))) {
					System.out.println("\n------------------------\n");
					System.out.printf("Date of service: %s\n"
							+ "Date and time received by computer: %s %s\n"
							+ "Member Name: %s\n"
							+ "Member Number: %s \n"
							+ "Service Code: %s\n"
							+ "Fee to be paid: %s\n", pdos.get(j), pdateEntered.get(j), ptimeEntered.get(j), pmemname.get(j), pmemnumber.get(j), pservicecode.get(j), pfeetobepaid.get(j));		
					numOfConsultations += 1;
					totalFeeForWeek = totalFeeForWeek + Double.parseDouble( pfeetobepaid.get(i));
					//Clearing from array once used its value.
					pname.remove(j);
					pnum.remove(j);
					pstreet.remove(j);
					pcity.remove(j);
					pstate.remove(j);
					pzip.remove(j);
					pdos.remove(j);
					pdateEntered.remove(j);
					ptimeEntered.remove(j);
					pmemname.remove(j);
					pmemnumber.remove(j);
					pservicecode.remove(j);
					pfeetobepaid.remove(j);
				}	
			}		
			System.out.printf("Total Numbers of consultations with members: %d\n"
							+ "Total fee for week: $%f\n", numOfConsultations, totalFeeForWeek);
			numOfConsultations = 0;
			totalFeeForWeek = 0;
		}
		
		//Write Provider Reports file.
		writeProviderReportsToFile();
		}
		//generates summayreports.
		else if (checkWhichReport == "SummaryReport" && generateForThis == "0"){
			int numOfConsultationsEachHad = 0;
			double totalFeeForWeekEachHad = 0.0;
			int numOfOverallConsultions = 0;
			double totalFeelOverall = 0.0;
			for (int i = 0; i < providerTracker.size(); i++) {
				System.out.print("\n----------------------------XXX-------------------------\n");
				System.out.printf("Provider Name : %s\n"
								+ "Provider Number : %s\n", pname.get(i), pnum.get(i));
				numOfOverallConsultions += 1;
				numOfConsultationsEachHad += 1;	
				totalFeeForWeekEachHad = totalFeeForWeekEachHad + Double.parseDouble( pfeetobepaid.get(i));
				totalFeelOverall = totalFeeForWeekEachHad + totalFeelOverall;
				for (int j = i+1; j < pname.size(); j++) {
					if (pnum.get(j).equals(pnum.get(i))) {
						numOfConsultationsEachHad += 1;
						totalFeeForWeekEachHad = totalFeeForWeekEachHad + Double.parseDouble( pfeetobepaid.get(i));
						numOfOverallConsultions += 1;
						totalFeelOverall = totalFeeForWeekEachHad + totalFeelOverall;
						//Clearing all values once used.
						pname.remove(j);
						pnum.remove(j);
						pstreet.remove(j);
						pcity.remove(j);
						pstate.remove(j);
						pzip.remove(j);
						pdos.remove(j);
						pdateEntered.remove(j);
						ptimeEntered.remove(j);
						pmemname.remove(j);
						pmemnumber.remove(j);
						pservicecode.remove(j);
						pfeetobepaid.remove(j);
					}	
				}		
				System.out.printf("Number of consultation: %d\n"
						+ "Total fee for week: %f\n", numOfConsultationsEachHad, totalFeeForWeekEachHad);
				totalFeeForWeekEachHad = 0.0;
				numOfConsultationsEachHad = 0;
			}
			System.out.println("\nxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx\n");
			System.out.printf("Total Number of providers who provided service: %d\n"
					+ "Total Numbers of consultations: %d \n"
					+ "Over all fee: %f \n", providerTracker.size(), numOfOverallConsultions, totalFeelOverall);
			writeSummaryReportToFile();
		}
		//Generating individual provider reports.
		else if (checkWhichReport == "ProviderReport" && generateForThis != "0") {
			int numOfConsultations = 0;
			double totalFeeForWeek = 0.0;
			for (int i = 0; i < providerTracker.size(); i++) {
				if (pnum.get(i).equals(generateForThis)) {
				System.out.printf("\n---------------------------    %d      -----------\n", i);
				System.out.printf("Provider Name: %s\n"
						+ "Provider Number: %s\n"
						+ "Provider Address: %s\n"
						+ "%s %s, %s \n"
						+ "Date of service: %s\n"
						+ "Date and time received by computer: %s %s\n"
						+ "Member Name: %s\n"
						+ "Member Number: %s \n"
						+ "Service Code: %s\n"
						+ "Fee to be paid: %s\n",pname.get(i), pnum.get(i), pstreet.get(i), pcity.get(i), pstate.get(i), pzip.get(i), pdos.get(i), pdateEntered.get(i), ptimeEntered.get(i), pmemname.get(i), pmemnumber.get(i), pservicecode.get(i), pfeetobepaid.get(i));
				numOfConsultations += 1;			
				totalFeeForWeek = totalFeeForWeek + Double.parseDouble( pfeetobepaid.get(i));
				for (int j = i+1; j < pname.size(); j++) {
					if (pnum.get(j).equals(pnum.get(i))) {
						System.out.println("\n------------------------\n");
						System.out.printf("Date of service: %s\n"
								+ "Date and time received by computer: %s %s\n"
								+ "Member Name: %s\n"
								+ "Member Number: %s \n"
								+ "Service Code: %s\n"
								+ "Fee to be paid: %s\n", pdos.get(j), pdateEntered.get(j), ptimeEntered.get(j), pmemname.get(j), pmemnumber.get(j), pservicecode.get(j), pfeetobepaid.get(j));		
						numOfConsultations += 1;
						totalFeeForWeek = totalFeeForWeek + Double.parseDouble( pfeetobepaid.get(i));
						//Clearing array once used its value.
						pname.remove(j);
						pnum.remove(j);
						pstreet.remove(j);
						pcity.remove(j);
						pstate.remove(j);
						pzip.remove(j);
						pdos.remove(j);
						pdateEntered.remove(j);
						ptimeEntered.remove(j);
						pmemname.remove(j);
						pmemnumber.remove(j);
						pservicecode.remove(j);
						pfeetobepaid.remove(j);
					}	
				}		
				System.out.printf("Total Numbers of consultations with members: %d\n"
								+ "Total fee for week: $%f\n", numOfConsultations, totalFeeForWeek);
				numOfConsultations = 0;
				totalFeeForWeek = 0;
			}
			}
		}
	}
	
	//Poppulating array .... Pardon me for name switch.. 
	void printWithProviderInfoInReport() {
		pname.add(proName);
		pnum.add(proNum);
		pstreet.add(proStreet);
		pcity.add(proCity);
		pstate.add(proState);
		pzip.add(proZip);
		pdos.add(proDateOfService);
		pdateEntered.add(proDateEntered);
		ptimeEntered.add(proTimeEntered);
		pmemname.add(proMemberName);
		pmemnumber.add(proMemberNumber);
		pservicecode.add(proServiceCode);
		pfeetobepaid.add(proFeeToBePaid);
	}
	
	/**
	 *  Write Summary reports to the file..
	 */
	void writeSummaryReportToFile()  {
		int numOfConsultationsEachHad = 0;
		double totalFeeForWeekEachHad = 0.0;
		int numOfOverallConsultions = 0;
		double totalFeelOverall = 0.0;
		FileWriter fileWriter = null;
		try {
			fileWriter = new FileWriter("Summury_Report.txt"); //Creating summary_reports.txt
		} catch (IOException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
			}
	    PrintWriter printWriter = new PrintWriter(fileWriter);
	    
	    for (int i = 0; i < providerTracker.size(); i++) {
	    	printWriter.printf("\nProvider Name : %s\n"
	    		+ "Provider Number : %s\n", pname.get(i), pnum.get(i));
			numOfOverallConsultions += 1;
			numOfConsultationsEachHad += 1;	
			totalFeeForWeekEachHad = totalFeeForWeekEachHad + Double.parseDouble( pfeetobepaid.get(i));
			totalFeelOverall = totalFeeForWeekEachHad + totalFeelOverall;
			for (int j = i+1; j < pname.size(); j++) {
				if (pnum.get(j).equals(pnum.get(i))) {
					numOfConsultationsEachHad += 1;
					totalFeeForWeekEachHad = totalFeeForWeekEachHad + Double.parseDouble( pfeetobepaid.get(i));
					numOfOverallConsultions += 1;
					totalFeelOverall = totalFeeForWeekEachHad + totalFeelOverall;
					pname.remove(j);
					pnum.remove(j);
					pstreet.remove(j);
					pcity.remove(j);
					pstate.remove(j);
					pzip.remove(j);
					pdos.remove(j);
					pdateEntered.remove(j);
					ptimeEntered.remove(j);
					pmemname.remove(j);
					pmemnumber.remove(j);
					pservicecode.remove(j);
					pfeetobepaid.remove(j);
				}	
			}		
			printWriter.printf("Number of consultation: %d\n"
					+ "Total fee for week: %f\n", numOfConsultationsEachHad, totalFeeForWeekEachHad);
			totalFeeForWeekEachHad = 0.0;
			numOfConsultationsEachHad = 0;
		} 
	    printWriter.printf("\n\n\tTotal Number of providers who provided service: %d\n"
				+ "\tTotal Numbers of consultations: %d \n"
				+ "\tOver all fee: %f \n", providerTracker.size(), numOfOverallConsultions, totalFeelOverall);
	   printWriter.close();	
	}
	
	/**Writing provider reports to the file Generating Email Provider Reports folder and in that
	 * Storing all provider reports files as 
	 */
	
	void writeProviderReportsToFile () {
		int numOfConsultations = 0;
		double totalFeeForWeek = 0.0;
		for (int i = 0; i < providerTracker.size(); i++) {
			File f = new File(System.getProperty("user.dir")+"/Email Provider Reports/"+ pnum.get(i) + ".txt");
		    if(!f.getParentFile().exists()){
		        f.getParentFile().mkdirs();
		    }
		    //Remove if clause if you want to overwrite file
		    if(!f.exists()){
		        try {
		            f.createNewFile();
		        } catch (Exception e) {
		            e.printStackTrace();
		        }
		    }
		    try {
		        //dir will change directory and specifies file name for writer
		        File dir = new File(f.getParentFile(), f.getName());
		        PrintWriter writer = new PrintWriter(dir);
		        writer.printf("Provider Name: %s\n"
						+ "Provider Number: %s\n"
						+ "Provider Address: %s\n"
						+ "%s %s, %s \n"
						+ "Date of service: %s\n"
						+ "Date and time received by computer: %s %s\n"
						+ "Member Name: %s\n"
						+ "Member Number: %s \n"
						+ "Service Code: %s\n"
						+ "Fee to be paid: %s\n",pname.get(i), pnum.get(i), pstreet.get(i), pcity.get(i), pstate.get(i), pzip.get(i), pdos.get(i), pdateEntered.get(i), ptimeEntered.get(i), pmemname.get(i), pmemnumber.get(i), pservicecode.get(i), pfeetobepaid.get(i));
		        numOfConsultations += 1;			
				totalFeeForWeek = totalFeeForWeek + Double.parseDouble( pfeetobepaid.get(i));
				for (int j = i+1; j < pname.size(); j++) {
					if (pnum.get(j).equals(pnum.get(i))) {
						writer.printf("Date of service: %s\n"
								+ "Date and time received by computer: %s %s\n"
								+ "Member Name: %s\n"
								+ "Member Number: %s \n"
								+ "Service Code: %s\n"
								+ "Fee to be paid: %s\n", pdos.get(j), pdateEntered.get(j), ptimeEntered.get(j), pmemname.get(j), pmemnumber.get(j), pservicecode.get(j), pfeetobepaid.get(j));		
						numOfConsultations += 1;
						totalFeeForWeek = totalFeeForWeek + Double.parseDouble( pfeetobepaid.get(i));
						pname.remove(j);
						pnum.remove(j);
						pstreet.remove(j);
						pcity.remove(j);
						pstate.remove(j);
						pzip.remove(j);
						pdos.remove(j);
						pdateEntered.remove(j);
						ptimeEntered.remove(j);
						pmemname.remove(j);
						pmemnumber.remove(j);
						pservicecode.remove(j);
						pfeetobepaid.remove(j);
					}	
				}		
				writer.printf("Total Numbers of consultations with members: %d\n"
								+ "Total fee for week: $%f\n", numOfConsultations, totalFeeForWeek);
				numOfConsultations = 0;
				totalFeeForWeek = 0;
		        writer.close();
		    } catch (Exception e) {
		        e.printStackTrace();
		    } 
		}
	} 
	
	/**This function receives in memberNumber and generate Individual member reports.
	 * 
	 * @param memberNumber is receives in member number to generate individual member reports
	 */
	void generateIndividualMemberReports (String memberNumber) {
		eftData.clear();
		while (EFTDataFile.hasNext()) {
            String s = EFTDataFile.next();
            		//System.out.printf("%s", s);
            		eftData.add(s);
			}
        for (int i = 0; i < eftData.size(); i++) {
        		 if (eftData.get(i).equals(memberNumber)) {
        			memDateOfService = eftData.get(i-4);
        			memNum = memberNumber;
        			findMemberInfoForMemberReport(memberNumber);
        			memProviderName = findProviderNameForMemberReport(eftData.get(i-2));
        			findServiceNameForIndMemberReport(eftData.get(i+2));   			
        		}
        }
	}
	
	/**This function receives in memberNumber and generate Individual member reports.
	 * 
	 * @param serviceCoce is receives in servicecode to serch service name
	 */
	public void findServiceNameForIndMemberReport(String serviceCode) {
		String code, Servicename = "";
		try {
			ProviderDirectoryFile = new Scanner (new File ("ProviderDirectory.txt"));
		}
		catch (Exception e) {
			System.out.println("No file named Provider Directory exists in system.");
		}
		while (ProviderDirectoryFile.hasNext()) {
			code = ProviderDirectoryFile.next();
			if (code.equals(serviceCode)) {
				Servicename =  ProviderDirectoryFile.next();
				memServiceName = Servicename;
				printAllMemberReport();
			}
		}
	}
}
