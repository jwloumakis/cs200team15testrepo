package project4;

import java.io.IOException;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

public class Main {
	public static void main(String[] args) throws IOException {
		Scanner scanner = new Scanner(System.in);
		int choice = 0;
		do {
			StringBuilder choicesOption = new StringBuilder();
			choicesOption.append("\n**************Welcome to ChocAn!**************\n");
			choicesOption.append("Please Select from one of the options below: \n");
			choicesOption.append("1. Perform Terminal Actions. \n");
			choicesOption.append("2. Run Interactive Mode. \n");
			choicesOption.append("3. Run Main Accounting Procedure. \n");
			choicesOption.append("0. Exit System.");
	
			System.out.println(choicesOption.toString());
			String testIn = scanner.nextLine();
			int n;
			if (testIn.matches("^[0-9]+$") && ((n = Integer.parseInt(testIn)) >= 0) && (n <= 3)) choice = n;
			else {
				System.out.println("Invalid input. Please enter a single-digit integer (0-3).");
				try {
					TimeUnit.SECONDS.sleep((long) 2.5);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			
			switch (choice) {
			case 0:
				System.out.println("Thank you for using ChocAn software!");
				try {
					TimeUnit.SECONDS.sleep(1);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				break;
			case 1:
				PerformTerminalActions pta = new PerformTerminalActions();
				pta.activateTerminal();
				break;
			case 2:
				OperatorTerminal ot = new OperatorTerminal();
				ot.start();
				break;
			case 3:
				MainAccountingProcedure map = new MainAccountingProcedure();
				map.Choice();
				break;
			}
	} while (choice != 0);
	
	/** This is the code for how BillingProcedure.java should be implemented
	 * 	
	 *  BillingProcedure x = new BillingProcedure(123456789); //provider number input
		x.setMemberNumber(111111111);  //takes MemberNumber parameter
		x.getDate(); //returns string
		x.getServiceCode(); //returns int
		while(!x.validateService()) {  //validateService returns boolean
	      x.getServiceCode();
		}
		x.writeDataToDisk();
		x.getServiceFee(); //returns double
		x.fillOutForm();
	 */
	
	
	}	
}