package project4;

import static org.junit.Assert.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Scanner;

import org.junit.Test;

public class OperatorTerminalTest {

	private OperatorTerminal operatorTerminal = new OperatorTerminal();
    
	//Checking is memberlist file exists, but it dont exists.
    @Test
	 public void test() {
    	File memList = new File("MEMBERLIST.txt");  
	assertFalse(!memList.exists());
	
	}
     
    //Reaturning boolean value if that 123456789 is available.
    @Test
    public void test1() throws IOException {
    		File memList = new File("MemberList.txt");
    		boolean result = operatorTerminal.checkNum(123456789, memList);
    		assertEquals(result,true);
    }
    
    //Adding member info to memberList file 
	ArrayList<String> addMember = new ArrayList<String>(); 
    @Test
    public void test2() throws IOException {
    	File memList = new File("MemberList.txt");
    	int num = operatorTerminal.generateNewNumber(memList);
    	addMember.add("Jack");
    	addMember.add("Sparrow");
    	addMember.add("123 Carabian Cruice");
    	addMember.add("Birmingham");
    	addMember.add("En");
    	addMember.add(Integer.toString(35401));
	boolean x = operatorTerminal.writeToFile(num, addMember);
	assertEquals(x, true);
    }
    

}
