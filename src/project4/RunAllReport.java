package project4;
import java.io.File;
import java.io.FileReader;
import java.util.Scanner;

public class RunAllReport {
	boolean printThis = false;
	int choice = 0;
	Scanner scanner = new Scanner(System.in);
	private Scanner MemberListFile;
	private Scanner ProviderListFile;
	ReadFileDataForReports readThis = new ReadFileDataForReports();
	public void runAllMemberReports() {
		System.out.println("All member reports ");
		readThis.openFileForGenerateAllMemberReports();
		readThis.readFileForGenerateAllMemberReports();
		readThis.closeFileForGenerateAllMemberReports();	
	}
	
	public void runAllProviderReports() {
		System.out.println("All provider reports");
		readThis.openFileForGenerateAllProviderReports();
		readThis.readFileForGenerateAllProviderReports("ProviderReport","0");
		readThis.closeFileForGenerateAllProviderReports();
	}
	
	public void runAllSummaryReports() {
		System.out.println("All summary reports");
		readThis.openFileForGenerateAllProviderReports();
		readThis.readFileForGenerateAllProviderReports("SummaryReport","0");
		readThis.closeFileForGenerateAllProviderReports();
	}
	
	public void runIndividualMemberReports() {		
		int i = 0;
		do {
			System.out.println("Enter in valid member number to generate report: \n");
			choice = scanner.nextInt();
			String line, numberExists = "false";
			try {
				MemberListFile = new Scanner (new File ("MemberList"));
				}
			catch (Exception e) {
				System.out.println("No file named EFTData exists in system.");
			}
			String generateForThis = Integer.toString(choice);
			while (MemberListFile.hasNext()) {
				line = MemberListFile.next();
				if (line.equals(generateForThis)) {
					numberExists = "true"; 
					break;
				}
			}
		if (numberExists.equals("true")) {
			readThis.openFileForGenerateAllMemberReports();
			readThis.generateIndividualMemberReports(generateForThis);
			readThis.closeFileForGenerateAllMemberReports();	
			i = 1;
		}
		} while (i != 1);
	}
	
	public void runIndividualProviderReports() {		
		int i = 0;
		do {
			System.out.println("Enter in valid provider number to generate report: \n");
			choice = scanner.nextInt();
			String line, numberExists = "false";
			try {
				ProviderListFile = new Scanner (new File ("ProviderList"));
				}
			catch (Exception e) {
				System.out.println("No file named EFTData exists in system.");
			}
			String generateForThis = Integer.toString(choice);
			while (ProviderListFile.hasNext()) {
				line = ProviderListFile.next();
				if (line.equals(generateForThis)) {
					numberExists = "true"; 
					break;
				}
			}
		if (numberExists.equals("true")) {
			readThis.openFileForGenerateAllProviderReports();
			readThis.readFileForGenerateAllProviderReports("ProviderReport",generateForThis);
			readThis.closeFileForGenerateAllProviderReports();
			i = 1;
		}
		} while (i != 1);
	}
}
