package project4;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

/**
 * This is the Perform Terminal Actions class. It allows 
 * a Provider to validate a Member, look up the Provider
 * Directory, or bill a Member.
 * @author wabeaumont
 *
 */

public class PerformTerminalActions {
	
	File proList = new File("ProviderList.txt");
	File memList = new File("MemberList.txt");
	File proDir = new File("ProviderDirectory.txt");
	private int proNum;
	private int memNum;
	
	/**
	 * This method takes a code entered by a Provider into the terminal
	 * to make sure that the person using it is authorized to do so.
	 * @param num -- The Provider number being validated
	 * @param proList -- The list of Providers
	 * @return
	 */
	
 	public boolean validateProvider(int num, File proList) {
		Scanner scan = null;
		try {
			scan = new Scanner(proList);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		while (scan.hasNextLine()) {
			int curNum = scan.nextInt();
			if (curNum == num) {
				System.out.println("Validated\n");
				return true;
			}
		}
		System.out.println("Invalid number\n");
		return false;
	}
	
 	/**
 	 * This method creates a visual menu so that the Provider can 
 	 * choose an action to perform.
 	 * @throws FileNotFoundException -- If the file entered does not exist
 	 */
 	
 	public void terminalGUI() throws FileNotFoundException {
 		Scanner scanner = new Scanner(System.in);
 		int choice = 0;
 		do {
 		StringBuilder choicesOption = new StringBuilder();
		choicesOption.append("\n**************Welcome, Provider**************\n");
		choicesOption.append("Please Select from one of the options below: \n");
		choicesOption.append("1. Verify Member. \n");
		choicesOption.append("2. Request Provider Directory \n");
		choicesOption.append("3. Activate Billing Procedure. \n");
		choicesOption.append("0. To return to main screen.");
		
		System.out.println(choicesOption.toString());
		try {
			TimeUnit.SECONDS.sleep(1);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		String testIn = scanner.nextLine();
		int n;
		if (testIn.matches("^[0-9]+$") && ((n = Integer.parseInt(testIn)) >= 0) && (n <= 3)) choice = Integer.parseInt(testIn);
		else {
			System.out.println("Invalid input. Please enter a single-digit integer.");
			try {
				TimeUnit.SECONDS.sleep((long) 2.5);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			terminalGUI();
		}
		switch (choice) {
		case 0:
			System.out.println("Returning to main screen");
			break;
		case 1:
			validateMember(memList);
			break;
		case 2:
			requestProviderDirectory(proDir);
			break;
		case 3:
			activateBillingProcedure();
			break;
		} 	
	}
	while (choice != 0);
 	}
 	
 	/**
 	 * This method begins operations at the terminal for the Provider.
 	 * It verifies the Provider and then presents the Provider with
 	 * a list of actions that can be taken.
 	 * @throws FileNotFoundException -- If the file entered does not exist
 	 */
 	
	public void activateTerminal() throws FileNotFoundException {
		Scanner login = new Scanner(System.in);
		System.out.println("Enter provider number: ");
		int num = login.nextInt();
		proNum = num;
		if (validateProvider(num, proList) == true) {
			terminalGUI();
		}
	}

	/**
	 * This method allows the Provider to check the validity of a Member number.
	 * If the number does not exist or if it correlates to a suspended Member,
	 * then the Member will be shown to be invalidated and why.
	 * @param memList -- The list of Members and their statuses
	 * @return -- Gives a boolean value
	 */
	
	private boolean validateMember(File memList) {
		Scanner scan = null;
		try {
			scan = new Scanner(memList);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("Enter Member number: ");
		Scanner member = new Scanner(System.in);
		int num = member.nextInt();
		memNum = num;
		while (scan.hasNextLine()) {
			int curNum = scan.nextInt();
			if (curNum == num) {
				if (scan.next() == "isSuspended") {
					System.out.println("Member suspended\n");
				}
				else {
					System.out.println("Validated\n");
					return true;
				}
			}
		}
		System.out.println("Invalid number\n");
		return false;
	}
	
	/**
	 * This method allows the Provider to print out a list of services
	 * that a Member can be charged for.
	 * @param providerDirectory -- The list of services
	 */
	
	public void requestProviderDirectory(File providerDirectory) {
		Scanner scan = null;
		try {
			scan = new Scanner(providerDirectory);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("\n");
		while (scan.hasNextLine()) {
			String curString = scan.nextLine();
			System.out.println(curString + "\n");
		}
	}
	
	/**
	 * This method invokes the Billing Procedure class, allowing the 
	 * Provider to charge a Member for services provided.
	 * @throws FileNotFoundException -- If the file entered does not exist
	 */
	
	public void activateBillingProcedure() throws FileNotFoundException {
		Scanner scan = new Scanner(System.in);
		BillingProcedure x = new BillingProcedure(proNum);
		if (validateMember(memList) == false) {
			validateMember(memList);
		}
		x.setMemberNumber(memNum);  //takes MemberNumber parameter
		x.getDate(); //returns string
		
		do {
			while (!(x.lookupServiceCode(x.getServiceCode(), proDir)));
		} while (!x.userValidate(proDir));
		x.writeDataToDisk();
		try {
			x.getServiceFee();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} //returns double
		x.fillOutForm();
	}

}
