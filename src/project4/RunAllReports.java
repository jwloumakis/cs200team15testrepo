package project4;
import java.io.File;
import java.io.FileReader;
import java.util.Scanner;

/**
* Runs the all of the reports for the program
* 
* @author cdberry2
* @version 1.0
*/
public class RunAllReports {
	/**
	 * Scanner and report definitions for the class
	 */
	boolean printThis = false;
	int choice = 0;
	Scanner scanner = new Scanner(System.in);
	private Scanner MemberListFile;
	private Scanner ProviderListFile;
	ReadFileDataForReports readThis = new ReadFileDataForReports();
	
	/**
	 * Opens the EFT Data file, reads all of the member
	 *  data for report generation, closes the EFT Data file
	 */
	public void runAllMemberReports() {
		System.out.println("All member reports ");
		readThis.openEFTDataFile();
		readThis.readFileForGenerateAllMemberReports();
		readThis.closeFileForGenerateAllMemberReports();	
	}
	
	/**
	 * Opens the EFT Data file, reads all of the provider
	 * data for report generation, closes the EFT Data file
	 */
	public void runAllProviderReports() {
		System.out.println("All provider reports");
		readThis.openEFTDataFile();
		readThis.readFileForGenerateAllProviderReports("ProviderReport","0");
		readThis.closeFileForGenerateAllProviderReports();
	}
	
	/**
	 * Opens the EFT Data file, reads all of the member and provider
	 * data for summary report generation, closes the EFT Data file
	 */
	public void runAllSummaryReports() {
		System.out.println("All summary reports");
		readThis.openEFTDataFile();
		readThis.readFileForGenerateAllProviderReports("SummaryReport","0");
		readThis.closeFileForGenerateAllProviderReports();
	}
	
	/**
	 * runs an individual member report for a specified member
	 * by scanning the member list EFT Data
	 */
	public void runIndividualMemberReports() {		
		int i = 0;
		do {
			System.out.println("Enter in valid member number to generate report: \n");
			choice = scanner.nextInt();
			String line, numberExists = "false";
			try {
				MemberListFile = new Scanner (new File ("MemberList.txt"));
				}
			catch (Exception e) {
				System.out.println("No file named MemberList exists in system.");
			}
			String generateForThis = Integer.toString(choice);
			while (MemberListFile.hasNext()) {
				line = MemberListFile.next();
				if (line.equals(generateForThis)) {
					numberExists = "true"; 
					break;
				}
			}
		// if the member number exists in the member list, generates a member report
		if (numberExists.equals("true")) {
			readThis.openEFTDataFile();
			readThis.generateIndividualMemberReports(generateForThis);
			readThis.closeFileForGenerateAllMemberReports();	
			i = 1;
		}
		// if the member number does not exist in the member list, does not generate a member report
		else {
			System.out.println("Please enter in valid number");
		}
		} while (i != 1);
	}
	
	/**
	 * runs an individual member report for a specified provider
	 * by scanning the provider list for the EFT Data
	 */
	public void runIndividualProviderReports() {		
		int i = 0;
		do {
			System.out.println("Enter in valid provider number to generate report: \n");
			choice = scanner.nextInt();
			String line, numberExists = "false";
			try {
				ProviderListFile = new Scanner (new File ("ProviderList.txt"));
				}
			catch (Exception e) {
				System.out.println("No file named ProviderList exists in system.");
			}
			String generateForThis = Integer.toString(choice);
			while (ProviderListFile.hasNext()) {
				line = ProviderListFile.next();
				if (line.equals(generateForThis)) {
					numberExists = "true"; 
					break;
				}
			}
		// if the provider number exists in the provider list, generates the provider report
		if (numberExists.equals("true")) {
			readThis.openEFTDataFile();
			readThis.readFileForGenerateAllProviderReports("ProviderReport",generateForThis);
			readThis.closeFileForGenerateAllProviderReports();
			i = 1;
		}
		// if the provider number does not exist in the provider list, doesn't generate
		else {
			System.out.println("Please enter in valid provider number");
		}
		} while (i != 1);
	}
}
