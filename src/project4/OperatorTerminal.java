package project4;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * This class is used by a provider who will
 * run the Interactive Mode procedure.
 * 
 * @author jwloumakis
 *
 */

public class OperatorTerminal {
	
	ArrayList<String> addMember = new ArrayList<String>();
	
	/**
	 * Main class runs this method to initiate
	 * Run Interactive Mode. This method gives
	 * the options to add, update, and delete
	 * members and providers.
	 * 
	 * 
	 * @throws IOException if the scanner cannot be opened.
	 */ 
	
	public void start() throws IOException {
		Scanner scanner = new Scanner(System.in);
		int choice = 0;
		do {
		StringBuilder choicesOption = new StringBuilder();
		choicesOption.append("\n**************Welcome to Interactive Mode!**************\n");
		choicesOption.append("Please Select from one of the options below: \n");
		choicesOption.append("1. Add a Member \n");
		choicesOption.append("2. Add a Provider. \n");
		choicesOption.append("3. Update a Member. \n");
		choicesOption.append("4. Update a Provider. \n");
		choicesOption.append("5. Delete a Member. \n");
		choicesOption.append("6. Delete a Provider. \n");
		choicesOption.append("0. To return to main screen.");
		
			System.out.println(choicesOption.toString());
			String testIn = scanner.nextLine();
			int n;
			if (testIn.matches("^[0-9]+$") && ((n = Integer.parseInt(testIn)) >= 0) && (n <= 6)) choice = Integer.parseInt(testIn);
			else {
				System.out.println("Invalid input. Please enter a single-digit integer.");
				try {
					TimeUnit.SECONDS.sleep((long) 2.5);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				start();
			}
			switch (choice) {
			case 0:
				System.out.println("Returning to main screen");
				break;
			case 1:
				addMember();
				break;
			case 2:
				addProvider();
				break;
			case 3:
				updateMember();
				break;
			case 4: 
				updateProvider();
				break;
			case 5: 
				deleteMember();
				break;
			case 6:
				deleteProvider();
				break;
			} 	
		}while (choice != 0);
	}

	/**
	 * Generates a new random number from a given input list
	 * and tests to see if that number is taken.
	 * 
	 * @param userList: the member or provider input list to check from
	 * @throws IOException when userList cannot be opened
	 */
	public int generateNewNumber(File userList) throws IOException {
		int randomNum = ThreadLocalRandom.current().nextInt(100000000, 1000000000);
		Boolean isOldNum = checkNum(randomNum, userList);
		if (!isOldNum) return randomNum;
		return generateNewNumber(userList);
		}
	
	/**
	 * Checks to see if a given number is already taken.
	 * 
	 *  @param num: the number to check for availability
	 *  @param userList: the file to check num against
	 *  @throws IOException when userList cannot be opened
	 */
	public boolean checkNum(int num, File userList) throws IOException {
		BufferedReader reader = new BufferedReader(new FileReader(userList));
		String findNum = Integer.toString(num);
		String s = null;
		while ((s = reader.readLine()) != null) {
			String trimmedLine = s.trim();
			if (trimmedLine.equals(findNum)) { reader.close(); return true; }
		}
		reader.close();
		return false;
	}
	
	/**
	 * Starts the process for adding a member
	 * to the ChocAn software.
	 * 
	 * @throws IOException if the scanner cannot be opened and/or the member list cannot be opened.
	 */
	private void addMember() throws IOException {

		Scanner scan = new Scanner(System.in);
		System.out.print("Enter First Name: ");
		String fName = scan.nextLine();
		
		System.out.print("Enter Last Name: ");
		String lName = scan.nextLine();
		
		System.out.print("Enter Street Address: ");
		String address = scan.nextLine();
		
		System.out.print("Enter City: ");
		String city = scan.nextLine();
		
		System.out.print("Enter Two Letter State Code: ");
		String state = scan.nextLine();
		while (state.length() != 2) {
			System.out.println("Sorry. State Code must be 2 letters.");
			state = scan.nextLine();
		}
		state = state.toUpperCase();
		
		System.out.print("Enter ZIP Code: ");
		int zip = 0;
		do {
			String testIn = scan.nextLine();
			int n;
			if (testIn.matches("^[0-9]+$") && ((n = Integer.parseInt(testIn)) >= 10000) && (n <= 99999)) zip = n;
			else {
				System.out.println("Invalid input. Please enter a five-digit integer ZIP code.");
				try {
					TimeUnit.SECONDS.sleep((long) 2.5);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		} while (zip == 0);
		
		addMember.add(fName);
		addMember.add(lName);
		addMember.add(address);
		addMember.add(city);
		addMember.add(state);
		addMember.add(Integer.toString(zip));
		File memList = new File("MemberList.txt");
		FileWriter writer = new FileWriter(memList, true);
		
		int memNum = generateNewNumber(memList);
		
		writer.close();
		boolean x = writeToFile(memNum, addMember);
		if (x == true) {
			System.out.println("Added Member!");
		}
		else {
			System.out.println("Didnt added member :(");
		}
		return;
	}
	
	public boolean writeToFile (int memNum, ArrayList<String>addMember) throws IOException {
		File memList = new File("MemberList.txt");
		FileWriter writer = new FileWriter(memList, true);
		writer.write("\n"+ memNum + "\n" + addMember.get(0) + '\n' + addMember.get(1)  + '\n' + addMember.get(2) + '\n' + addMember.get(3) + '\n' + addMember.get(4) + '\n' + addMember.get(5)+ '\n' + "ACTIVE" + '\n' + "END" + '\n');
		writer.close();
		addMember.clear();
		return true;
	}
	

	/**
	 * Starts the process for deleting a member
	 * from the ChocAn software.
	 * 
	 * @throws IOException if the scanner cannot be opened and/or the member list cannot be opened.
	 */
	private void deleteMember() throws IOException {
		System.out.print("Please enter the Member Number for the Member to be deleted: ");
		Scanner scanner = new Scanner(System.in);
		String testIn = scanner.nextLine();
		int n, num = 0;
		if (testIn.matches("^[0-9]+$") && ((n = Integer.parseInt(testIn)) >= 100000000) && (n <= 999999999)) num = n;
		else {
			System.out.println("Invalid input. Please ensure you are entering a valid nine-digit Member Number.");
			try {
				TimeUnit.SECONDS.sleep((long) 2.5);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			deleteMember();
		}
		
		File memList = new File("MemberList.txt");
		File tempMemList = new File("TempMemberList.txt");
		FileWriter writer = new FileWriter(tempMemList);
		BufferedReader reader = new BufferedReader(new FileReader(memList));
		
		String findNum = Integer.toString(num);
		String s = null;
		while ((s = reader.readLine()) != null) {
			String trimmedLine = s.trim();
			if (trimmedLine.equals(findNum)) {
				for (int i = 0; i < 8; i++) reader.readLine();
				continue;
			}
			writer.write(s + System.getProperty("line.separator"));
		}
		reader.close();
		writer.close();
		if (!memList.delete()) System.out.println("Could not delete file.");
		if (!tempMemList.renameTo(memList)) System.out.println("Could not rename file.");
		return;
	}
	
	/**
	 * Starts the process for updating a member's
	 * information and updates the ChocAn software.
	 * 
	 * @throws IOException if the scanner cannot be opened and/or the member list cannot be opened.
	 */
	private void updateMember() throws IOException {
		System.out.print("Please enter the Member Number for the Member to be updated: ");
		File memList = new File("MemberList.txt");
		
		//Checks if the entered number is a 9-digit integer.
		Scanner scanner = new Scanner(System.in);
		String testIn = scanner.nextLine();
		int n, num = 0;
		if (testIn.matches("^[0-9]+$") && ((n = Integer.parseInt(testIn)) >= 100000000) && (n <= 999999999)) num = n;
		else {
			System.out.println("Invalid input. Please ensure you are entering a valid nine-digit Member Number.");
			try {
				TimeUnit.SECONDS.sleep((long) 2.5);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			updateMember();
		}
		
		//Checks if the number is valid.
		if(!checkNum(num, memList)) {
			System.out.println("Sorry. The Member Number entered was not found!");
			updateMember();
		}
		
		
		int choice = 0;
		do {
			StringBuilder choicesOption = new StringBuilder();
			choicesOption.append("Please Select an aspect to update from one of the options below: \n");
			choicesOption.append("1. Member's First Name \n");
			choicesOption.append("2. Member's Last Name \n");
			choicesOption.append("3. Member's Address \n");
			choicesOption.append("4. Member's City \n");
			choicesOption.append("5. Member's State \n");
			choicesOption.append("6. Member's ZIP Code \n");
			choicesOption.append("7. Member's Activity Status \n");
			choicesOption.append("0. Return to main screen.");
			
			System.out.println(choicesOption.toString());
			try {
				TimeUnit.SECONDS.sleep(1);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			testIn = scanner.nextLine();
			if (testIn.matches("^[0-9]+$") && ((n = Integer.parseInt(testIn)) >= 0) && (n <= 7)) choice = n;
			else {
				System.out.println("Invalid input. Please enter a single-digit integer.");
				try {
					TimeUnit.SECONDS.sleep((long) 2.5);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			
			File tempMemList = new File("TempMemberList.txt");
			FileWriter writer = new FileWriter(tempMemList);
			BufferedReader reader = new BufferedReader(new FileReader(memList));
			switch (choice) {
				case 0:
					System.out.println("Returning to main screen");
					reader.close();
					writer.close();
					break;
				case 1:
					System.out.print("Enter new First Name: ");
					String newFirstName = scanner.nextLine();
				
					String findNum1 = Integer.toString(num);
					String s1 = null;
					while ((s1 = reader.readLine()) != null) {
						String trimmedLine = s1.trim();
						if (trimmedLine.equals(findNum1)) {
							writer.write(s1 + System.getProperty("line.separator"));
							reader.readLine();
							writer.write(newFirstName + System.getProperty("line.separator"));
							continue;
						}
						writer.write(s1 + System.getProperty("line.separator"));
					}
					reader.close();
					writer.close();
					if (!memList.delete()) System.out.println("Could not delete file.");
					if (!tempMemList.renameTo(memList)) System.out.println("Could not rename file.");
					break;
				case 2:
					System.out.print("Enter new Last Name: ");
					String newLastName = scanner.nextLine();
					
					String findNum2 = Integer.toString(num);
					String s2 = null;
					while ((s2 = reader.readLine()) != null) {
						String trimmedLine = s2.trim();
						if (trimmedLine.equals(findNum2)) {
							writer.write(s2 + System.getProperty("line.separator"));
							s2 = reader.readLine(); writer.write(s2 + System.getProperty("line.separator"));
							reader.readLine();
							writer.write(newLastName + System.getProperty("line.separator"));
							continue;
						}
						writer.write(s2 + System.getProperty("line.separator"));
					}
					reader.close();
					writer.close();
					if (!memList.delete()) System.out.println("Could not delete file.");
					if (!tempMemList.renameTo(memList)) System.out.println("Could not rename file.");
					break;
				case 3:
					System.out.print("Enter new Address: ");
					String newAddress = scanner.nextLine();
					
					String findNum3 = Integer.toString(num);
					String s3 = null;
					while ((s3 = reader.readLine()) != null) {
						String trimmedLine = s3.trim();
						if (trimmedLine.equals(findNum3)) {
							writer.write(s3 + System.getProperty("line.separator"));
							s3 = reader.readLine(); writer.write(s3 + System.getProperty("line.separator"));
							s3 = reader.readLine(); writer.write(s3 + System.getProperty("line.separator"));
							reader.readLine();
							writer.write(newAddress + System.getProperty("line.separator"));
							continue;
						}
						writer.write(s3 + System.getProperty("line.separator"));
					}
					reader.close();
					writer.close();
					if (!memList.delete()) System.out.println("Could not delete file.");
					if (!tempMemList.renameTo(memList)) System.out.println("Could not rename file.");
					break;
				case 4: 
					System.out.print("Enter new City: ");
					String newCity = scanner.nextLine();
					
					String findNum4 = Integer.toString(num);
					String s4 = null;
					while ((s4 = reader.readLine()) != null) {
						String trimmedLine = s4.trim();
						if (trimmedLine.equals(findNum4)) {
							writer.write(s4 + System.getProperty("line.separator"));
							s4 = reader.readLine(); writer.write(s4 + System.getProperty("line.separator"));
							s4 = reader.readLine(); writer.write(s4 + System.getProperty("line.separator"));
							s4 = reader.readLine(); writer.write(s4 + System.getProperty("line.separator"));
							reader.readLine();
							writer.write(newCity + System.getProperty("line.separator"));
							continue;
						}
						writer.write(s4 + System.getProperty("line.separator"));
					}
					reader.close();
					writer.close();
					if (!memList.delete()) System.out.println("Could not delete file.");
					if (!tempMemList.renameTo(memList)) System.out.println("Could not rename file.");
					break;
				case 5: 
					System.out.print("Enter new two-digit State Code: ");
					String newState = scanner.nextLine();
					while (newState.length() != 2) {
						System.out.println("Sorry. State Code must be 2 letters.");
						newState = scanner.nextLine();
					}
					newState = newState.toUpperCase();
					
					String findNum5 = Integer.toString(num);
					String s5 = null;
					while ((s5 = reader.readLine()) != null) {
						String trimmedLine = s5.trim();
						if (trimmedLine.equals(findNum5)) {
							writer.write(s5 + System.getProperty("line.separator"));
							s5 = reader.readLine(); writer.write(s5 + System.getProperty("line.separator"));
							s5 = reader.readLine(); writer.write(s5 + System.getProperty("line.separator"));
							s5 = reader.readLine(); writer.write(s5 + System.getProperty("line.separator"));
							s5 = reader.readLine(); writer.write(s5 + System.getProperty("line.separator"));
							reader.readLine();
							writer.write(newState + System.getProperty("line.separator"));
							continue;
						}
						writer.write(s5 + System.getProperty("line.separator"));
					}
					reader.close();
					writer.close();
					if (!memList.delete()) System.out.println("Could not delete file.");
					if (!tempMemList.renameTo(memList)) System.out.println("Could not rename file.");
					break;
				case 6:
					System.out.print("Enter new ZIP Code: ");
					int newZIP = 0;
					do {
						String test = scanner.nextLine();
						int p;
						if (test.matches("^[0-9]+$") && ((p = Integer.parseInt(test)) >= 10000) && (p <= 99999)) newZIP = p;
						else {
							System.out.println("Invalid input. Please enter a five-digit integer ZIP code.");
							try {
								TimeUnit.SECONDS.sleep((long) 2.5);
							} catch (InterruptedException e) {
								e.printStackTrace();
							}
						}
					} while (newZIP == 0);
					
					String findNum6 = Integer.toString(num);
					String s6 = null;
					while ((s6 = reader.readLine()) != null) {
						String trimmedLine = s6.trim();
						if (trimmedLine.equals(findNum6)) {
							writer.write(s6 + System.getProperty("line.separator"));
							s6 = reader.readLine(); writer.write(s6 + System.getProperty("line.separator"));
							s6 = reader.readLine(); writer.write(s6 + System.getProperty("line.separator"));
							s6 = reader.readLine(); writer.write(s6 + System.getProperty("line.separator"));
							s6 = reader.readLine(); writer.write(s6 + System.getProperty("line.separator"));
							s6 = reader.readLine(); writer.write(s6 + System.getProperty("line.separator"));
							reader.readLine();
							writer.write(newZIP + System.getProperty("line.separator"));
							continue;
						}
						writer.write(s6 + System.getProperty("line.separator"));
					}
					reader.close();
					writer.close();
					if (!memList.delete()) System.out.println("Could not delete file.");
					if (!tempMemList.renameTo(memList)) System.out.println("Could not rename file.");
					break;
				case 7:
					System.out.print("Enter new member status (Active or Suspended): ");
					String newStatus = scanner.nextLine();
					while (!(newStatus.toUpperCase().contains("ACTIVE") || newStatus.toUpperCase().contains("SUSPENDED"))) {
						System.out.println("Sorry. Status must be either \"Active\" or \"Suspended\"");
						newStatus = scanner.nextLine();
					}
					newStatus = newStatus.toUpperCase();
					
					String findNum7 = Integer.toString(num);
					String s7 = null;
					while ((s7 = reader.readLine()) != null) {
						String trimmedLine = s7.trim();
						if (trimmedLine.equals(findNum7)) {
							writer.write(s7 + System.getProperty("line.separator"));
							s7 = reader.readLine(); writer.write(s7 + System.getProperty("line.separator"));
							s7 = reader.readLine(); writer.write(s7 + System.getProperty("line.separator"));
							s7 = reader.readLine(); writer.write(s7 + System.getProperty("line.separator"));
							s7 = reader.readLine(); writer.write(s7 + System.getProperty("line.separator"));
							s7 = reader.readLine(); writer.write(s7 + System.getProperty("line.separator"));
							s7 = reader.readLine(); writer.write(s7 + System.getProperty("line.separator"));
							reader.readLine();
							writer.write(newStatus + System.getProperty("line.separator"));
							continue;
						}
						writer.write(s7 + System.getProperty("line.separator"));
					}
					reader.close();
					writer.close();
					if (!memList.delete()) System.out.println("Could not delete file.");
					if (!tempMemList.renameTo(memList)) System.out.println("Could not rename file.");
					break;
			}
		}while (choice != 0);
		
		return;
	}
	
	/**
	 * Starts the process for adding a provider
	 * to the ChocAn software.
	 * 
	 * @throws IOException if the scanner cannot be opened and/or the provider list cannot be opened.
	 */
	private void addProvider() throws IOException {
		Scanner scan = new Scanner(System.in);
		System.out.print("Enter First Name: ");
		String fName = scan.nextLine();
		
		System.out.print("Enter Last Name: ");
		String lName = scan.nextLine();
		
		System.out.print("Enter Street Address: ");
		String address = scan.nextLine();
		
		System.out.print("Enter City: ");
		String city = scan.nextLine();
		
		System.out.print("Enter Two Letter State Code: ");
		String state = scan.nextLine();
		while (state.length() != 2) {
			System.out.println("Sorry. State Code must be 2 letters.");
			state = scan.nextLine();
		}
		state = state.toUpperCase();
		
		System.out.print("Enter ZIP Code: ");
		int zip = 0;
		do {
			String testIn = scan.nextLine();
			int n;
			if (testIn.matches("^[0-9]+$") && ((n = Integer.parseInt(testIn)) >= 10000) && (n <= 99999)) zip = n;
			else {
				System.out.println("Invalid input. Please enter a five-digit integer ZIP code.");
				try {
					TimeUnit.SECONDS.sleep((long) 2.5);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		} while (zip == 0);
		
		File provList = new File("ProviderList.txt");
		FileWriter writer = new FileWriter(provList, true);
		
		int provNum = generateNewNumber(provList);
		
		writer.write(provNum + "\n" + fName + '\n' + lName  + '\n' + address + '\n' + city + '\n' + state + '\n' + zip + '\n' + "END" + '\n');
		
		writer.close();
		
		System.out.println("Provider added!");
		return;
	}
	
	/**
	 * Starts the process for deleting a provider
	 * from the ChocAn software.
	 * 
	 * @throws IOException if the scanner cannot be opened and/or the provider list cannot be opened.
	 */
	private void deleteProvider() throws IOException {
		System.out.print("Please enter the Provider Number for the Provider to be deleted: ");
		Scanner scanner = new Scanner(System.in);
		String testIn = scanner.nextLine();
		int n, num = 0;
		if (testIn.matches("^[0-9]+$") && ((n = Integer.parseInt(testIn)) >= 100000000) && (n <= 999999999)) num = Integer.parseInt(testIn);
		else {
			System.out.println("Invalid input. Please ensure you are entering a valid nine-digit Provider Number.");
			try {
				TimeUnit.SECONDS.sleep((long) 2.5);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			deleteProvider();
		}
		
		File provList = new File("ProviderList.txt");
		File tempProvList = new File("TempProviderList.txt");
		FileWriter writer = new FileWriter(tempProvList);
		BufferedReader reader = new BufferedReader(new FileReader(provList));
		
		String findNum = Integer.toString(num);
		String s = null;
		while ((s = reader.readLine()) != null) {
			String trimmedLine = s.trim();
			if (trimmedLine.equals(findNum)) {
				for (int i = 0; i < 7; i++) reader.readLine();
				continue;
			}
			writer.write(s + System.getProperty("line.separator"));
		}
		reader.close();
		writer.close();
		if (!provList.delete()) System.out.println("Could not delete file.");
		if (!tempProvList.renameTo(provList)) System.out.println("Could not rename file.");
		return;
	}
	
	/**
	 * Starts the process for updating a provider's
	 * information and updates the ChocAn software.
	 * 
	 * @throws IOException if the scanner cannot be opened and/or the provider list cannot be opened.
	 */
	private void updateProvider() throws IOException{
		System.out.print("Please enter the Member Number for the Member to be updated: ");
		File provList = new File("ProviderList.txt");
		
		//Checks if the entered number is a 9-digit integer.
		Scanner scanner = new Scanner(System.in);
		String testIn = scanner.nextLine();
		int n, num = 0;
		if (testIn.matches("^[0-9]+$") && ((n = Integer.parseInt(testIn)) >= 100000000) && (n <= 999999999)) num = n;
		else {
			System.out.println("Invalid input. Please ensure you are entering a valid nine-digit Provider Number.");
			try {
				TimeUnit.SECONDS.sleep((long) 2.5);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			updateProvider();
		}
		
		//Checks if the number is valid.
		if(!checkNum(num, provList)) {
			System.out.println("Sorry. The Provider Number entered was not found!");
			updateProvider();
		}
		
		
		int choice = 0;
		do {
			StringBuilder choicesOption = new StringBuilder();
			choicesOption.append("Please Select an aspect to update from one of the options below: \n");
			choicesOption.append("1. Provider's First Name \n");
			choicesOption.append("2. Provider's Last Name \n");
			choicesOption.append("3. Provider's Address \n");
			choicesOption.append("4. Provider's City \n");
			choicesOption.append("5. Provider's State \n");
			choicesOption.append("6. Provider's ZIP Code \n");
			choicesOption.append("0. Return to main screen.");
			
			System.out.println(choicesOption.toString());
			try {
				TimeUnit.SECONDS.sleep(1);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			testIn = scanner.nextLine();
			if (testIn.matches("^[0-9]+$") && ((n = Integer.parseInt(testIn)) >= 0) && (n <= 6)) choice = Integer.parseInt(testIn);
			else {
				System.out.println("Invalid input. Please enter a single-digit integer.");
				try {
					TimeUnit.SECONDS.sleep((long) 2.5);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			
			File tempProvList = new File("TempProviderList.txt");
			FileWriter writer = new FileWriter(tempProvList);
			BufferedReader reader = new BufferedReader(new FileReader(provList));
			switch (choice) {
				case 0:
					System.out.println("Returning to main screen");
					reader.close();
					writer.close();
					break;
				case 1:
					System.out.print("Enter new First Name: ");
					String newFirstName = scanner.nextLine();
				
					String findNum1 = Integer.toString(num);
					String s1 = null;
					while ((s1 = reader.readLine()) != null) {
						String trimmedLine = s1.trim();
						if (trimmedLine.equals(findNum1)) {
							writer.write(s1 + System.getProperty("line.separator"));
							reader.readLine();
							writer.write(newFirstName + System.getProperty("line.separator"));
							continue;
						}
						writer.write(s1 + System.getProperty("line.separator"));
					}
					reader.close();
					writer.close();
					if (!provList.delete()) System.out.println("Could not delete file.");
					if (!tempProvList.renameTo(provList)) System.out.println("Could not rename file.");
					break;
				case 2:
					System.out.print("Enter new Last Name: ");
					String newLastName = scanner.nextLine();
					
					String findNum2 = Integer.toString(num);
					String s2 = null;
					while ((s2 = reader.readLine()) != null) {
						String trimmedLine = s2.trim();
						if (trimmedLine.equals(findNum2)) {
							writer.write(s2 + System.getProperty("line.separator"));
							s2 = reader.readLine(); writer.write(s2 + System.getProperty("line.separator"));
							reader.readLine();
							writer.write(newLastName + System.getProperty("line.separator"));
							continue;
						}
						writer.write(s2 + System.getProperty("line.separator"));
					}
					reader.close();
					writer.close();
					if (!provList.delete()) System.out.println("Could not delete file.");
					if (!tempProvList.renameTo(provList)) System.out.println("Could not rename file.");
					break;
				case 3:
					System.out.print("Enter new Address: ");
					String newAddress = scanner.nextLine();
					
					String findNum3 = Integer.toString(num);
					String s3 = null;
					while ((s3 = reader.readLine()) != null) {
						String trimmedLine = s3.trim();
						if (trimmedLine.equals(findNum3)) {
							writer.write(s3 + System.getProperty("line.separator"));
							s3 = reader.readLine(); writer.write(s3 + System.getProperty("line.separator"));
							s3 = reader.readLine(); writer.write(s3 + System.getProperty("line.separator"));
							reader.readLine();
							writer.write(newAddress + System.getProperty("line.separator"));
							continue;
						}
						writer.write(s3 + System.getProperty("line.separator"));
					}
					reader.close();
					writer.close();
					if (!provList.delete()) System.out.println("Could not delete file.");
					if (!tempProvList.renameTo(provList)) System.out.println("Could not rename file.");
					break;
				case 4: 
					System.out.print("Enter new City: ");
					String newCity = scanner.nextLine();
					
					String findNum4 = Integer.toString(num);
					String s4 = null;
					while ((s4 = reader.readLine()) != null) {
						String trimmedLine = s4.trim();
						if (trimmedLine.equals(findNum4)) {
							writer.write(s4 + System.getProperty("line.separator"));
							s4 = reader.readLine(); writer.write(s4 + System.getProperty("line.separator"));
							s4 = reader.readLine(); writer.write(s4 + System.getProperty("line.separator"));
							s4 = reader.readLine(); writer.write(s4 + System.getProperty("line.separator"));
							reader.readLine();
							writer.write(newCity + System.getProperty("line.separator"));
							continue;
						}
						writer.write(s4 + System.getProperty("line.separator"));
					}
					reader.close();
					writer.close();
					if (!provList.delete()) System.out.println("Could not delete file.");
					if (!tempProvList.renameTo(provList)) System.out.println("Could not rename file.");
					break;
				case 5: 
					System.out.print("Enter new two-digit State Code: ");
					String newState = scanner.nextLine();
					while (newState.length() != 2) {
						System.out.println("Sorry. State Code must be 2 letters.");
						newState = scanner.nextLine();
					}
					newState = newState.toUpperCase();
					
					String findNum5 = Integer.toString(num);
					String s5 = null;
					while ((s5 = reader.readLine()) != null) {
						String trimmedLine = s5.trim();
						if (trimmedLine.equals(findNum5)) {
							writer.write(s5 + System.getProperty("line.separator"));
							s5 = reader.readLine(); writer.write(s5 + System.getProperty("line.separator"));
							s5 = reader.readLine(); writer.write(s5 + System.getProperty("line.separator"));
							s5 = reader.readLine(); writer.write(s5 + System.getProperty("line.separator"));
							s5 = reader.readLine(); writer.write(s5 + System.getProperty("line.separator"));
							reader.readLine();
							writer.write(newState + System.getProperty("line.separator"));
							continue;
						}
						writer.write(s5 + System.getProperty("line.separator"));
					}
					reader.close();
					writer.close();
					if (!provList.delete()) System.out.println("Could not delete file.");
					if (!tempProvList.renameTo(provList)) System.out.println("Could not rename file.");
					break;
				case 6:
					System.out.print("Enter new ZIP Code: ");
					int newZIP = 0;
					do {
						String test = scanner.nextLine();
						int p;
						if (test.matches("^[0-9]+$") && ((p = Integer.parseInt(test)) >= 10000) && (p <= 99999)) newZIP = p;
						else {
							System.out.println("Invalid input. Please enter a five-digit integer ZIP code.");
							try {
								TimeUnit.SECONDS.sleep((long) 2.5);
							} catch (InterruptedException e) {
								e.printStackTrace();
							}
						}
					} while (newZIP == 0);
					
					String findNum6 = Integer.toString(num);
					String s6 = null;
					while ((s6 = reader.readLine()) != null) {
						String trimmedLine = s6.trim();
						if (trimmedLine.equals(findNum6)) {
							writer.write(s6 + System.getProperty("line.separator"));
							s6 = reader.readLine(); writer.write(s6 + System.getProperty("line.separator"));
							s6 = reader.readLine(); writer.write(s6 + System.getProperty("line.separator"));
							s6 = reader.readLine(); writer.write(s6 + System.getProperty("line.separator"));
							s6 = reader.readLine(); writer.write(s6 + System.getProperty("line.separator"));
							s6 = reader.readLine(); writer.write(s6 + System.getProperty("line.separator"));
							reader.readLine();
							writer.write(newZIP + System.getProperty("line.separator"));
							continue;
						}
						writer.write(s6 + System.getProperty("line.separator"));
					}
					reader.close();
					writer.close();
					if (!provList.delete()) System.out.println("Could not delete file.");
					if (!tempProvList.renameTo(provList)) System.out.println("Could not rename file.");
					break;
			}
		}while (choice != 0);
		
		return;
	}
}